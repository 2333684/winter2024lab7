public class Card {
	private String suit;
	private String value;
	
	public Card (String suit, String value)
	{
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit()
	{
		return this.suit;
	}
	public String getValue()
	{
		return this.value;
	}
	
	public String toString()
	{
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore()
	{
		double cardTotalRanking = 0;
		double valueRanking = 0;
		double suitRanking = 0;
		
		String [] valueList = {"Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
		
		for (int i = 0; i < valueList.length; i++){
			
			if(valueList[i] == this.value){
				valueRanking = i+1;
			}
		}
		
		if(this.suit == "Hearts"){
			suitRanking = 0.4;
		}else if(this.suit == "Spades"){
			suitRanking = 0.3;
		}else if(this.suit == "Diamonds"){
			suitRanking = 0.2;
		}else if(this.suit == "Clubs"){
		suitRanking = 0.1;
		} 
		
		cardTotalRanking = suitRanking + valueRanking;
		
		return cardTotalRanking;
	}
	
}