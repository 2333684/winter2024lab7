public class SimpleWar{
	public static void main (String [] args )
	{
		Deck cardDeck = new Deck();
		cardDeck.shuffle();
		System.out.println(cardDeck);
		
		int playerOnePoint = 0;
		int playerTwoPoint = 0;
		int numOfRound = 0;
		/* 	
		System.out.println("Player One currently has " + playerOnePoint + " points");
		System.out.println("Player Two currently has " + playerTwoPoint + " points" + "\n");
		
		Card cardOne = cardDeck.drawTopCard();
		Card cardTwo = cardDeck.drawTopCard();
		
		double cardOneScore = cardOne.calculateScore();
		double cardTwoScore = cardTwo.calculateScore();
		
		System.out.println("CardOne score is: " + cardOneScore);
		System.out.println("CardOTwo score is: " + cardTwoScore);
		
		if(cardOneScore > cardTwoScore){
			playerOnePoint++;
		}else {
			playerTwoPoint++;
		}
		
		System.out.println("\n" + "Player One now has " + playerOnePoint + " points");
		System.out.println("Player Two now has " + playerTwoPoint + " points");
		System.out.println("***********************************************"); */
		
		while(cardDeck.length() > 0){
			
			numOfRound ++;
			System.out.println("Round: " + numOfRound);
			System.out.println("Player One currently has " + playerOnePoint + " points");
			System.out.println("Player Two currently has " + playerTwoPoint + " points" + "\n");
		
			Card cardOne = cardDeck.drawTopCard();
			Card cardTwo = cardDeck.drawTopCard();
		
			double cardOneScore = cardOne.calculateScore();
			double cardTwoScore = cardTwo.calculateScore();
			
			System.out.println("Player One drew: " + cardOne + " which has a score of: " + cardOneScore);
			System.out.println("Player Two drew: " + cardTwo + " which has a scire of: " + cardTwoScore);
		
			if(cardOneScore > cardTwoScore){
				playerOnePoint++;
			}else {
				playerTwoPoint++;
			}
		
			System.out.println("\n" + "Player One now has " + playerOnePoint + " points");
			System.out.println("Player Two now has " + playerTwoPoint + " points");
			System.out.println("***********************************************" + "\n");
		}
		
		if(playerOnePoint > playerTwoPoint){
			System.out.println("Congrats on winning Player One with: " + playerOnePoint + " points!");
		}else if (playerOnePoint < playerTwoPoint){
			System.out.println("Congrats on winning Player Two with: " + playerTwoPoint + " points!");
		}else {
			System.out.println("The game is a tied.");
		}
	}
}